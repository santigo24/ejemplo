const Sequelize = require("sequelize");
const usuarios = require("../models/usuarios");
const respuestas = require("../models").Respuestas;

module.exports = {
  List(_, res) {
    return respuestas
      .findAll({})
      .then((respuestas) => res.status(200).send(respuestas))
      .catch((error) => res.status(400).send(error));
  },
  ListAt(req, res) {
    return respuestas
      .findAll({
        where: {
          id: req.params.id,
        },
      })
      .then((respuestas) => res.status(200).send(respuestas))
      .catch((error) => res.status(400).send(error));
  },

  DeleteRespuestas(req, res) {
    return respuestas
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((respuestas) => res.sendStatus(respuestas))
      .catch((error) => res.status(400).send(error));
  },

  UpdateRespuestas(req, res) {
    return respuestas
      .update(
        {
          nombre_usuario: req.body.nombre_usuario,
          cantidad_like: req.body.cantidad_like,
          cantidad_no_like: req.body.cantidad_no_like,
          fecha_respuestas: req.body.fecha_respuestas,
          contenido: req.body.contenido,
          id_usuarios: req.body.id_usuarios,
          id_preguntas: req.body.id_preguntas,
        },
        {
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.json(result);
      });
  },
  CreateRespuestas(req, res) {
    return respuestas
      .create({
        nombre_usuario: req.params.nombre_usuario,
        cantidad_like: req.params.cantidad_like,
        cantidad_no_like: req.params.cantidad_no_like,
        fecha_respuestas: req.params.fecha_respuestas,
        contenido: req.params.contenido,
        id_usuarios: req.params.id_usuarios,
        id_preguntas: req.params.id_preguntas,
      })
      .then((respuestas) => res.status(200).send(respuestas))
      .catch((error) => res.status(400).send(error));
  },
};
