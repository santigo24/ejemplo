var express = require('express');
var router = express.Router();

const Sequelize = require('sequelize');
const respuestas = require('../models').Respuestas;
const usuarios = require('../models').Usuarios;


const EstadisticasController = require('../Controllers/EstadisticasController');
const EstudiosController = require('../Controllers/EstudiosController');
const PreguntasController = require('../Controllers/PreguntasController');
const RespuestasController = require('../Controllers/RespuestasController');
const UsuariosController = require('../Controllers/UsuariosController');
const RegistroMultimediaController = require('../Controllers/RegistroMultimediaController');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/estadisticas/',EstadisticasController.List);
router.get('/api/estadisticas/id/:id',EstadisticasController.ListAt);
router.delete('/api/estadisticas/id/:id',EstadisticasController.DeleteEstadisticas);
router.patch('/api/estadisticas/id/:id',EstadisticasController.UpdateEstadisticas);
router.post('/api/estadisticas/:enviadas/:respondidas/:puntuacion/:id_usuarios',EstadisticasController.CreateEstadisticas);

router.get('/api/estudios',EstudiosController.List);
router.get('/api/estudiosid/:id',EstudiosController.ListAt);
router.delete('/api/estudiosid/:id',EstudiosController.DeleteEstudios);
router.patch('/api/estudiosid/:id',EstudiosController.UpdateEstudios);
router.post('/api/estudios:tipo_nivel_escolaridad/:nombre_titulo/:descripcion_experiencia/:id_usuarios',EstudiosController.CreateEstudios);

router.get('/api/preguntas/',PreguntasController.List);
router.get('/api/preguntas/id/:id',PreguntasController.ListAt);
router.delete('/api/preguntas/id/:id',PreguntasController.DeletePreguntas);
router.patch('/api/preguntas/id/:id',PreguntasController.UpdatePreguntas);
router.post('/api/preguntas/:titulo/:descripcion/:fecha_publicacion/:id_usuarios',PreguntasController.CreatePreguntas);

router.get('/api/respuestas/',RespuestasController.List);
router.get('/api/respuestas/id/:id',RespuestasController.ListAt);
router.delete('/api/respuestas/id/:id',RespuestasController.DeleteRespuestas);
router.patch('/api/respuestas/id/:id',RespuestasController.UpdateRespuestas);
router.post('/api/respuestas/:nombre_usuario/:cantidad_like/:cantidad_no_like/:fecha_respuestas/:contenido/:id_usuarios/:id_preguntas',RespuestasController.CreateRespuestas);

router.get('/api/usuarios/',UsuariosController.List);
router.get('/api/usuarios/id/:id',UsuariosController.ListAt);
router.delete('/api/usuarios/id/:id',UsuariosController.DeleteUsuarios);
router.patch('/api/usuarios/id/:id',UsuariosController.UpdateUsuarios);
router.post('/api/usuarios/:nombres/:apellidos/:fecha_nacimiento/:direccion/:genero/:telefono/:email/:password',UsuariosController.CreateUsuarios);

router.get('/api/registroMultimedia/',RegistroMultimediaController.List);
router.get('/api/registroMultimedia/id/:id',RegistroMultimediaController.ListAt);
router.delete('/api/registroMultimedia/id/:id',RegistroMultimediaController.DeleteRegistroMultimedia);
router.patch('/api/registroMultimedia/id/:id',RegistroMultimediaController.UpdateRegistroMultimedia);
router.post('/api/registroMultimedia/:nombre_archivo/:directorio_archivo/:id_preguntas',RegistroMultimediaController.CreateRegistroMultimedia);

// Consultas Kevin Rendon
router.get('/Usuarios/Respuestas',UsuariosController.ListarUsuariosRespuetas);
router.get('/Usuarios/Estudios/Estadisticas',UsuariosController.ListarUsuariosEstudiosEstadisticas);

module.exports = router;




