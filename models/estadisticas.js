"use strict";
const { Model } = require("sequelize");
const Usuarios = require("./usuarios");
module.exports = (sequelize, DataTypes) => {
  class Estadisticas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Estadisticas.belongsTo(models.Usuarios, { foreignKey: "id_usuarios" });
    }
  }
  Estadisticas.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      enviadas: {
        type: DataTypes.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      respondidas: {
        type: DataTypes.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      puntuacion: {
        type: DataTypes.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      id_usuarios: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "usuarios", key: "id" },
      },
    },
    {
      sequelize,
      modelName: "Estadisticas",
      tableName: "estadisticas",
    }
  );
  return Estadisticas;
};
