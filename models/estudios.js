"use strict";
const { Model } = require("sequelize");
const Usuarios = require("./usuarios");
module.exports = (sequelize, DataTypes) => {
  class Estudios extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Estudios.belongsTo(models.Usuarios, { foreignKey: "id_usuarios" });
    }
  }
  Estudios.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      tipo_nivel_escolaridad: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      nombre_titulo: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      descripcion_experiencia: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      id_usuarios: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: { model: "usuarios", key: "id" },
      },
    },
    {
      sequelize,
      modelName: "Estudios",
      tableName: "estudios",
    }
  );
  return Estudios;
};
