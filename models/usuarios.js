"use strict";
const { Model } = require("sequelize");
const Estadisticas = require("./estadisticas");
const Estudios = require("./estudios");
const Preguntas = require("./preguntas");
const Respuestas = require("./respuestas");
module.exports = (sequelize, DataTypes) => {
  class Usuarios extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Usuarios.hasMany(models.Estadisticas, { foreignKey: "id_usuarios" });
      Usuarios.hasMany(models.Estudios, { foreignKey: "id_usuarios" });
      Usuarios.hasMany(models.Preguntas, { foreignKey: "id_usuarios" });
      Usuarios.hasMany(models.Respuestas, { foreignKey: "id_usuarios" });
    }
  }
  Usuarios.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      nombres: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      apellidos: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      fecha_nacimiento: {
        type: DataTypes.DATE,
        defaultValue: false,
        allowNull: false,
      },
      direccion: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      genero: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      telefono: {
        type: DataTypes.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "Usuarios",
      tableName: "usuarios",
    }
  );
  return Usuarios;
};
