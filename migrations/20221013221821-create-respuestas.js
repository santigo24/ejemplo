"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("respuestas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nombre_usuario: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      cantidad_like: {
        type: Sequelize.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      cantidad_no_like: {
        type: Sequelize.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      fecha_respuestas: {
        type: Sequelize.DATE,
        defaultValue: false,
        allowNull: false,
      },
      contenido: {
        type: Sequelize.STRING(200),
        defaultValue: false,
        allowNull: false,
      },
      id_usuarios: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: "usuarios",
          },
          key: "id",
        },
      },
      id_preguntas: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: "preguntas",
          },
          key: "id",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("respuestas");
  },
};
