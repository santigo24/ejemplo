"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("estudios", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      tipo_nivel_escolaridad: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      nombre_titulo: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      descripcion_experiencia: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      id_usuarios: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: {
            tableName: "usuarios",
          },
          key: "id",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("estudios");
  },
};
