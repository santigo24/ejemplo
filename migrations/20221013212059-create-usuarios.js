"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("usuarios", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nombres: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      apellidos: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      fecha_nacimiento: {
        type: Sequelize.DATE,
        defaultValue: false,
        allowNull: false,
      },
      direccion: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      genero: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      telefono: {
        type: Sequelize.INTEGER(100),
        defaultValue: false,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      password: {
        type: Sequelize.STRING(100),
        defaultValue: false,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("usuarios");
  },
};
