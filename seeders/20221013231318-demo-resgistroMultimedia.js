"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("registroMultimedia", [
      {
        nombre_archivo: "Imagen Maiz",
        directorio_archivo: "C:\Users\Public\Pictures\Sample Pictures\Chrysanthemum.jpg",
        id_preguntas: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("registroMultimedia", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
