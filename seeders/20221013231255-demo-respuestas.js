"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("respuestas", [
      {
        nombre_usuario: "kevin",
        cantidad_like: 5,
        cantidad_no_like: 2,
        fecha_respuestas: "2022-10-12",
        contenido: "El abono ayuda al crecimiento de las plantas y contribuye a mejorar o mantener muchas propiedades del suelo.",
        id_usuarios: 1,
        id_preguntas: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("repuestas", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
